<?php

/**
 * This is the model class for table "rectangle".
 *
 * The followings are the available columns in table 'rectangle':
 * @property integer $id
 * @property integer $height
 * @property integer $width
 * @property integer $color
 * @property double $square
 * @property string $create_date
 * @property string $last_change_date
 */
class Rectangle extends CActiveRecord
{

    protected $oldValues;


    public function afterDelete(){
        RectangleHistory::model()->deleteAllByAttributes(array('rectangle_id' => $this->id));
        return parent::afterDelete();
    }

    public function setAttributes($values, $safeOnly = true){
        if (!$this->isNewRecord){
            $this->oldValues = $this->attributes;
        }
        return parent::setAttributes($values, $safeOnly);
    }

    public function afterSave(){
        if (!empty($this->oldValues)){
            $history = new RectangleHistory();
            $history->attributes = $this->oldValues;
            $history->rectangle_id = $this->id;
            $history->create_date = $this->last_change_date;
            $history->save();
        }
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rectangle';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('height, width, color, square, create_date', 'required'),
			array('height, width, color', 'numerical', 'integerOnly'=>true),
			array('square', 'numerical'),
			array('last_change_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, height, width, color, square, create_date, last_change_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'colors' => array(self::BELONGS_TO, 'Colors', 'color')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'height' => 'Height',
			'width' => 'Width',
			'color' => 'Color',
			'square' => 'Square',
			'create_date' => 'Create Date',
			'last_change_date' => 'Last Change Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('height',$this->height);
		$criteria->compare('width',$this->width);
		$criteria->compare('color',$this->color);
		$criteria->compare('square',$this->square);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('last_change_date',$this->last_change_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array (
                'pageSize' => 11 //edit your number items per page here
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rectangle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getHistoryModel(){
        $history = new RectangleHistory();
        $history->rectangle_id = $this->id;
        return $history;
    }
}
