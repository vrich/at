<?php

class SiteController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionView(){
          //  header("Content-Type: text/javascript");
            header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

        $answer = array(
            'success' => true,
            'message' => "Loaded data",
            'data' => Users::getAll()
        );

        echo CJSON::encode($answer);
    }

    public function actionCreate(){
        header("Content-Type: text/javascript");

        $answer = array(
            'success' => false,
            'message' => "Created record"
        );

        $data = Yii::app()->request->getPost('Users');

        if (!is_null($data)){
            $user = new Users();
            $user->attributes = $data;
            if ($user->save()){
                $answer = array(
                    'success' => true,
                    'message' => "Loaded data",
                    'data' => $user->attributes
                );
            }
        }

        echo CJSON::encode($answer);
    }

    public function actionUpdate($id){
        header("Content-Type: text/javascript");

        $answer = array(
            'success' => false,
            'message' => "Created record"
        );

        $data = Yii::app()->request->getPost('Users');

        if (!is_null($data)){
            $user = new Users();
            $user->attributes = $data;
            if ($user->save()){
                $answer = array(
                    'success' => true,
                    'message' => "Data are synchronized",
                    'data' => $user->attributes
                );
            }
        }

        echo CJSON::encode($answer);
    }


}