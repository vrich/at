<?php
$this->breadcrumbs=array(
	'Rectangles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Rectangle','url'=>array('index')),
	array('label'=>'Create Rectangle','url'=>array('create')),
	array('label'=>'View Rectangle','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Rectangle','url'=>array('admin')),
);
?>

<h1>Update Rectangle <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>