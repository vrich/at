<?php
$this->breadcrumbs=array(
	'Rectangles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Rectangle','url'=>array('index')),
	array('label'=>'Create Rectangle','url'=>array('create')),
	array('label'=>'Update Rectangle','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Rectangle','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Rectangle','url'=>array('admin')),
);
?>

<h1>View Rectangle #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'height',
        'width',
        'color',
        'square',
        'create_date',
        'last_change_date',
    ),
)); ?>
