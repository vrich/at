<?php
/**
 * @var $model Rectangle
 * @var $bindJs Boolean
 *
 */

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'rectangle-create-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'well',
        'method' => 'post'
    )
));
/**
 * @var $form TbActiveForm
 */
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php
echo $form->errorSummary($model);
echo $form->textFieldRow($model, 'height', array('class' => 'span5'));
echo $form->textFieldRow($model, 'width', array('class' => 'span5'));
echo $form->dropDownListRow($model, 'color', CHtml::listData(Colors::model()->findAll(), 'id', 'name'));
echo $form->textFieldRow($model, 'square', array('class' => 'span5'));
?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'ajaxSubmit',
            'type' => 'primary',
            'url' => $this->createUrl('create'),
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'ajaxOptions' =>
            array(
                'update' => '#rectangle-form',
                'method' => 'post'
            ),
            'htmlOptions' => array(
                'id' => 'rectangle-submit-button'
            )
        )
    ); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type' => 'danger',
            'label' => 'Cancel',
            'htmlOptions' => array(
                'id' => 'rectangle-form-close'
            ),
            'url' => '#',
        )
    );

    ?>
    <script>
        $('#rectangle-form-close').click(function(){
            $('#rectangle-form').empty();
            return false;
        });

        <?php
            $url = $model->isNewRecord ? $this->createUrl('create') : $this->createUrl('update', array('id'=>$model->id));
        ?>
        jQuery('#rectangle-submit-button').unbind('click').bind('click', function () {jQuery.ajax({'method': 'post', 'type': 'POST', 'url': '<?php echo $url?>', 'cache': false, 'data': jQuery(this).parents("form").serialize(), 'success': function (html) {jQuery("#rectangle-form").html(html)}});return false;});
        <?php  ?>

    </script>

</div>

    <?php


    $this->endWidget();


    if ($refresh['history']){
        ?>
        <script>
            (function(){
               // refreshContainers();
                var id = jQuery.fn.yiiGridView.getSelection('rectangle-grid');
                if (id){
                    showHistory(id);
                }
            })();
        </script>

    <?php
    }

    if ($refresh['rectangle']){
        ?>
        <script>
            (function(){
                refreshContainers();
            })();
        </script>

    <?php
    }
    ?>

