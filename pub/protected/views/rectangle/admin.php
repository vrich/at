<?php

/**
 * @var $model Rectangle
 */

$this->breadcrumbs = array(
    'Rectangles' => array('index'),
    'Manage',
);
?>

<h1>Manage Rectangles</h1>

<div class="row-fluid">
    <div class="span8">
        <?php $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'rectangle-grid',
            'dataProvider' => $model->search(),
            'type' => 'condensed bordered', 
            'selectionChanged'=> 'function(id){
                $.get("'.$this->createUrl('update').'/"+$.fn.yiiGridView.getSelection(id), function(html){
                    $("#rectangle-form").html(html);
                });
                showHistory($.fn.yiiGridView.getSelection(id));
            }',
            'columns' => array('id','height','width','colors.name','square',
                array(
                    'name' => 'last_change_date',
                    'class' => 'DateColumn',
                    'format' => 'full'
                ),
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                    'afterDelete'=>'function(link,success,data){refreshContainers()}',
                ),
            ),
        ));
        ?>
        <div id="history"></div>
    </div>
    <div class="span4">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('app', 'New rectangle'),
            'buttonType' => 'ajaxLink',
            'type' => 'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            /*'size' => 'large', // null, 'large', 'small' or 'mini'*/
            'url' => $this->createUrl('create'),
            'icon' => 'plus white',
            'ajaxOptions' => array(
                'update' => '#rectangle-form'
            ),
            'htmlOptions' => array(
                'class' => 'pull-right'
            )
        ));

        ?>
        <div class="clearfix"></div>
        <div id="rectangle-form"></div>
        <div id="history-detail"></div>

    </div>
</div>

<script>
    function refreshContainers(){
        $('#rectangle-grid').yiiGridView('update');
        $('#history-detail, #history, #rectangle-form').empty();
    }

    function showHistory(id){
        $.get("<?php echo $this->createUrl('history')?>/" + id, function(html){
            $("#history").html(html);
        });
    }
</script>
