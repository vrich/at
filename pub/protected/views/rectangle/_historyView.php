
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'height',
        'width',
        'colors.name',
        'square',
        'create_date',
    ),
    'htmlOptions' => array(
        'class' => 'well'
    )
)); ?>
