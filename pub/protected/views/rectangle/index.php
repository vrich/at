<?php
$this->breadcrumbs=array(
	'Rectangles',
);

/*$this->menu=array(
	array('label'=>'Create Rectangle','url'=>array('create')),
	array('label'=>'Manage Rectangle','url'=>array('admin')),
);*/
?>

<h1>Rectangles</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'rectangle-grid',
    'dataProvider'=>$model->search(),
/*    'filter'=>$model,*/
    'columns'=>array(
        'id',
        'height',
        'width',
        'color.name',
        'square',
        /*
        'create_date',
        'last_change_date',
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>

<div id="rectangle-form"></div>


<script>
    /*$(document).ready(function(){
        $('#sidebar a').click(function(){
            $.get(this.href, function(html){
                $('#rectangle-form').html(html);
            });
            return false;
        });
    })*/
</script>
