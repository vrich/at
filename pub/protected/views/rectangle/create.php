<?php
/* @var $this Controller */
$this->breadcrumbs=array(
	'Rectangles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Rectangle','url'=>array('index')),
	array('label'=>'Manage Rectangle','url'=>array('admin')),
);
?>

<h1>Create Rectangle</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>