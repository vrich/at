<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'rectangle-history-grid',
    'dataProvider' => $model->search(),
    'type' => 'condensed bordered',
    'template' => '{items}',
    'columns' => array(
        array(
            'name' => 'create_date',
            'class' => 'DateColumn',
            'header' => 'Change date'
        ),
        'height', 'width', 'colors.name', 'square',
    ),
));
?>
<script>
    jQuery('#rectangle-history-grid').yiiGridView({'ajaxUpdate':['rectangle-history-grid'],'ajaxVar':'ajax','pagerClass':'pagination','loadingClass':'grid-view-loading','filterClass':'filters','tableClass':'items table table-condensed table-bordered','selectableRows':1,'enableHistory':false,'updateSelector':'{page}, {sort}','filterSelector':'{filter}','pageVar':'Rectangle_history_page','selectionChanged':function(id){
        $.get("<?php echo $this->createUrl('detail') ;?>/"+jQuery('#rectangle-history-grid').yiiGridView.getSelection(id), function(html){
            $("#history-detail").html(html);
        });
    }});
</script>