-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 22 2013 г., 12:25
-- Версия сервера: 5.5.32
-- Версия PHP: 5.5.4-1+debphp.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `at`
--

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `name`) VALUES
(6, 'Голубой'),
(4, 'Желтый'),
(3, 'Зеленый'),
(1, 'Красный'),
(5, 'Оранжевый'),
(2, 'Синий'),
(7, 'Фиолетовый');

-- --------------------------------------------------------

--
-- Структура таблицы `rectangle`
--

CREATE TABLE IF NOT EXISTS `rectangle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `square` float NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_change_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Дамп данных таблицы `rectangle`
--

INSERT INTO `rectangle` (`id`, `height`, `width`, `color`, `square`, `create_date`, `last_change_date`) VALUES
(77, 4, 4, 3, 16, '2013-10-21 20:55:04', '2013-10-22 05:53:00'),
(78, 4, 3, 4, 14, '2013-10-22 05:35:24', '2013-10-22 05:18:00'),
(79, 2, 2, 1, 4, '2013-10-22 05:42:13', '2013-10-22 08:38:00');

-- --------------------------------------------------------

--
-- Структура таблицы `rectangle_history`
--

CREATE TABLE IF NOT EXISTS `rectangle_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rectangle_id` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `square` float NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Дамп данных таблицы `rectangle_history`
--

INSERT INTO `rectangle_history` (`id`, `rectangle_id`, `height`, `width`, `color`, `square`, `create_date`) VALUES
(1, 62, 102, 101, 1, 123, '2013-10-21 19:36:00'),
(2, 59, 102, 101, 1, 123, '2013-10-21 19:36:00'),
(3, 59, 102, 101, 1, 123, '2013-10-21 19:36:00'),
(4, 61, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(5, 59, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(6, 60, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(7, 62, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(8, 59, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(9, 60, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(10, 63, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(11, 64, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(12, 75, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(13, 72, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(14, 71, 102, 101, 1, 1234, '2013-10-21 19:03:00'),
(15, 60, 102, 101, 1, 1234, '2013-10-21 19:31:00'),
(16, 59, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(17, 61, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(18, 60, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(19, 59, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(20, 62, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(21, 63, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(22, 64, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(23, 72, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(24, 75, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(25, 71, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(26, 75, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(27, 72, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(28, 71, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(29, 60, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(30, 62, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(31, 64, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(32, 61, 102, 101, 1, 1234, '2013-10-21 19:32:00'),
(33, 76, 11, 11, 4, 121, '2013-10-21 19:27:00'),
(34, 76, 11, 11, 1, 121, '2013-10-21 19:58:00'),
(35, 76, 11, 11, 6, 121, '2013-10-21 19:12:00'),
(36, 76, 11, 11, 4, 121, '2013-10-21 19:25:00'),
(37, 76, 10, 10, 7, 120, '2013-10-21 19:02:00'),
(38, 76, 10, 10, 7, 120, '2013-10-21 20:01:00'),
(39, 77, 1, 2, 3, 2, '2013-10-21 20:44:00'),
(40, 77, 2, 2, 3, 4, '2013-10-22 05:25:00'),
(41, 77, 4, 2, 3, 4, '2013-10-22 05:58:00'),
(42, 77, 5, 2, 3, 4, '2013-10-22 05:02:00'),
(43, 77, 6, 2, 3, 4, '2013-10-22 05:56:00'),
(44, 77, 6, 3, 3, 18, '2013-10-22 05:42:00'),
(45, 77, 7, 3, 3, 21, '2013-10-22 05:55:00'),
(46, 77, 1, 3, 3, 3, '2013-10-22 05:08:00'),
(47, 77, 3, 3, 3, 9, '2013-10-22 05:31:00'),
(48, 77, 4, 3, 3, 12, '2013-10-22 05:53:00'),
(49, 78, 1, 1, 4, 1, '2013-10-22 05:55:00'),
(50, 78, 2, 1, 4, 1, '2013-10-22 05:05:00'),
(51, 78, 2, 2, 4, 4, '2013-10-22 05:32:00'),
(52, 78, 3, 2, 4, 4, '2013-10-22 05:26:00'),
(53, 78, 3, 3, 4, 4, '2013-10-22 05:18:00'),
(54, 79, 1, 1, 1, 15, '2013-10-22 05:04:00'),
(55, 79, 2, 1, 1, 15, '2013-10-22 08:38:00');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1382350829),
('m131021_101911_create_rectangle_table', 1382350878),
('m131021_134807_create_colors', 1382363505),
('m131021_183619_create_rectangle_history', 1382381237);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
