<?php

class m131021_183619_create_rectangle_history extends CDbMigration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE IF NOT EXISTS `rectangle_history` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `rectangle_id` int(11) NOT NULL,
              `height` int(11) NOT NULL,
              `width` int(11) NOT NULL,
              `color` int(11) NOT NULL,
              `square` float NOT NULL,
              `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    }

    public function down()
    {
        echo "m131021_183619_create_rectangle_history does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}