<?php

class m131021_101911_create_rectangle_table extends CDbMigration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE IF NOT EXISTS `rectangle` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `height` int(11) NOT NULL,
              `width` int(11) NOT NULL,
              `color` int(11) NOT NULL,
              `square` float NOT NULL,
              `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `last_change_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");

    }

    public function down()
    {
        echo "m131021_101911_create_rectangle_table does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}