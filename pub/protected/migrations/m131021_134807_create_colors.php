<?php

class m131021_134807_create_colors extends CDbMigration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `colors` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `name` (`name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ');
        $this->execute("
            INSERT INTO `colors` (`id`, `name`) VALUES
            (6, 'Голубой'),
            (4, 'Желтый'),
            (3, 'Зеленый'),
            (1, 'Красный'),
            (5, 'Оранжевый'),
            (2, 'Синий'),
            (7, 'Фиолетовый');
        ");
    }

    public function down()
    {
        echo "m131021_134807_create_colors does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}