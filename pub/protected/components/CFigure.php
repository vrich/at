<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vdme
 * Date: 21.10.13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */

abstract class CFigure extends CComponent {


    /**
     * @var $prototype CActiveRecord
     */
    protected $prototype;

    /**
     * @var $height int
     */
    protected $height;

    /**
     * @var $width int
     */
    protected $width;

    /**
     * @var $color int - value from directory
     */
    protected $color;

    /**
     * @var $square float
     */
    protected $square;

    /**
     * @var $date int -  timestamp - result of time() function
     */
    protected $date;

    /**
     * @return null - calculate square of figure
     */
    abstract protected function calculateSquare();


    public function getHeight(){
        return $this->height;
    }

    public function getWidth(){
        return $this->width;
    }

    public function getColor(){
        return $this->color;
    }

    public function getSquare(){
        $this->calculateSquare();
        return $this->square;
    }

    public function getDate($format = null){
        if (is_null($format)){
            return $this->date;
        }
        return Yii::app()->dateFormatter->format($format, $this->date);
    }


    public function setHeight($h){
        $this->height = intval($h);
    }

    public function setWidth($w){
        $this->width = intval($w);
    }

    public function setColor($color_id){
        // @todo: validate value from directory
        $this->color = $color_id;
    }

    protected  function setDate(){
        $this->date = time();
    }

}