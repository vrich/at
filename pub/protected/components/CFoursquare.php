<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vdme
 * Date: 21.10.13
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */
class CFoursquare extends Rectangle {

    protected function calculateSquare(){
        $this->square = pow($this->width, 2) ;
    }

    public  function setHeight($val){
        $this->width = $this->height = intval($val);
    }

    public  function setWidth($val){
        $this->setHeight($val);
    }
}