<?php

class DateColumn extends CDataColumn {

    public $linkTemplate;
    public $format = 'medium';

    public function renderDataCell($row){
        $data = $this->grid->dataProvider->data[$row];
        $isLink = !empty($this->linkTemplate);
        echo CHtml::openTag('td');
        echo $isLink ? $this->getLink($data) : $this->getDate($data);
        echo CHtml::CloseTag('td');
    }

    protected function getDate($data){
       if ("0000-00-00 00:00:00" == $data->{$this->name}){
           $this->type = "html";
           return CHtml::tag('span',array('class'=>'null'), Yii::t('zii','Not set'));
       }
       return Yii::app()->dateFormatter->formatDateTime($data->{$this->name},$this->format,'full');
    }

    protected function getLink($data){
        $link = $this->linkTemplate;
        foreach ($data as $key => $value){
            $link = str_replace('{'.$key.'}',$value, $link);
        }
        $res = CHtml::openTag('a', array('href'=>$link));
        $res.= $this->getDate($data);
        $res.= CHtml::closeTag('a');
        return $res;
    }
}
