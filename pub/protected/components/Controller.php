<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

     /**
     * @var string the default layouts for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layouts. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * @var array data for template
     */
    public $context = array();

    public $url;
    protected $args;

    public function __construct($a, $b) {
        $this->url = Yii::app()->request->getPathInfo();
        parent::__construct($a, $b);
    }

    public function beforeAction($args) {
        $this->args = $this->getActionParams();
        return parent::beforeAction($args);
    }

    public function __get($key) {
        if (isset($this->args[$key])) {
            return $this->args[$key];
        }
        return parent::__get($key);
    }

    private $_assetsUrl;

    public function getAssetsUrl() {
        if (null !== $this->_assetsUrl) {
            return $this->_assetsUrl;
        }
        $assetsPath = Yii::getPathOfAlias('application.assets');
        $this->setAssetsUrl($assetsPath);
        return $this->_assetsUrl;
    }

    public function setAssetsUrl($path) {
        if (($assetsPath = realpath($path)) === false || !is_dir($assetsPath) || !is_writable($assetsPath))
            throw new CException(Yii::t('app', 'Assets path "{path}" is not valid. Please make sure it is a directory writable by the Web server process.',
                array('{path}' => $path)));
        $assetsUrl = Yii::app()->assetManager->publish($path, false, -1, YII_DEBUG);
        $this->_assetsUrl = $assetsUrl;
    }
}
