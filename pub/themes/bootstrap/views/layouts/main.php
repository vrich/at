<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/ext/resources/css/ext-all.css" />
    <script type="text/javascript" charset="utf-8" src="<?php echo Yii::app()->baseUrl; ?>/ext/ext-all.js"></script>
	<title><?php echo CHtml::encode(Yii::t('app', $this->pageTitle)); ?></title>
	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
				/*array(
					'label'=>Yii::t('app', 'Logout') . ' ('.Yii::app()->user->name.')', 
					'url'=>array('/site/logout'), 
					'visible'=>!Yii::app()->user->isGuest
				),*/
            ),
        ),
    ),
)); ?>
<div id="wrap">
<div id="navbar-dummy"></div>
<div class="container" id="page">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	<?php echo $content; ?>
	<div class="clear"></div>

</div><!-- page -->
    <div id="footer-dummy"></div>
</div><!-- #end of wrap -->


<div id="footer" class="navbar navbar-fixed-bottom">
    <div class="container">
        <div class="row">
            <div class="span5"> Test Application for AmsterdamTelecom</div>
            <div class="span5"> <small>22 окт 2013г.</small></div>
            <div class="span2"> <a href="http://www.emelyanov.me" target="_blank">www.emelyanov.me</a> &copy;  </div>
        </div>
    </div>
</div><!-- footer -->

</body>
</html>
